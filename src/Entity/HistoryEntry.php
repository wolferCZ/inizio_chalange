<?php


namespace App\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CompanySearchEntry
 * @package App\Entity
 * @ORM\Entity("company_search_entry", repositoryClass="App\Repository\HistoryEntryRepository")
 */
class HistoryEntry {

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", nullable=false, type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="ico", type="string", length=8, nullable=false)
     */
    private $ico;

    /**
     * @var DateTime
     * @ORM\Column(name="searched_on", type="datetime", nullable=false)
     */
    private $searchedOn;

    /**
     * @var string
     * @ORM\Column(name="company_name", type="text")
     */
    private $companyName;

    /**
     * @var string
     * @ORM\Column(name="result", type="text")
     */
    private $result;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIco(): string {
        return $this->ico;
    }

    /**
     * @param string $ico
     */
    public function setIco(string $ico): void {
        $this->ico = $ico;
    }

    /**
     * @return DateTime
     */
    public function getSearchedOn(): DateTime {
        return $this->searchedOn;
    }

    /**
     * @param DateTime $searchedOn
     */
    public function setSearchedOn(DateTime $searchedOn): void {
        $this->searchedOn = $searchedOn;
    }

    /**
     * @return string
     */
    public function getCompanyName(): string {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName(string $companyName): void {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getResult(): string {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult(string $result): void {
        $this->result = $result;
    }


}
