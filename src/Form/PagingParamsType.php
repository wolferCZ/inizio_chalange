<?php


namespace App\Form;


use App\To\PagingParams;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Pagination params.
 * Simple form for retrieving (i.e. mapping and validating) query (GET) API params.
 * NOTE: Actual validation is specified by annotation in PagingParams.
 *
 * @package App\Form
 */
class PagingParamsType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('sort', TextType::class)
            ->add('sortBy', TextType::class)
            ->add('limit', IntegerType::class)
            ->add('offset', IntegerType::class)
            ->setMethod('GET');
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'method' => 'GET',
            'data_class' => PagingParams::class
        ]);
    }
}
