<?php


namespace App\Ares\Client;


use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Ares API Client
 * @package App\Ares\Client
 */
class AresClient {


    /**
     * Fetch Company from Ares API by its ICO.
     * @param string $ico
     * @return string xml
     */
    public function getCompanyByIco(string $ico) {
        $client = new Client(['base_uri' => 'https://wwwinfo.mfcr.cz']);

        $response = $client->get('/cgi-bin/ares/darv_std.cgi', ['query' => ['ico' => $ico]]);

        if ($response->getStatusCode() != 200) {
            throw new HttpException(Response::HTTP_NOT_FOUND, sprintf("Company with '%s' not found.", $ico));
        }
        $xml = $response->getBody()->getContents();
        $companyXml = simplexml_load_string($xml);
        if ((int)$companyXml->xpath('/are:Ares_odpovedi/are:Odpoved/are:Pocet_zaznamu')[0] != 1) {
            throw new HttpException(Response::HTTP_NOT_FOUND, sprintf("Found '%s' companies with ICO '%s'.",
                (int)$companyXml->xpath('/are:Ares_odpovedi/are:Odpoved/are:Pocet_zaznamu')[0], $ico));
        }

        // NOTE: Ideally, I would like to map retrieved information to PHP with JMS (or similar).
        return $xml;
    }


}
