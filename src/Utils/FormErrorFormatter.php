<?php


namespace App\Utils;


use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;

/**
 * Class FormErrorFormatter
 * @package App\Utils
 */
class FormErrorFormatter {

    /**
     * Map Symfony form errors to simple array.
     *
     * Disclaimer: I needed quickly and simply add validation error to the API.
     * Inspired by https://stackoverflow.com/a/24606082
     * @param FormInterface $form
     * @return array
     */
    public static function formatFormErrors(FormInterface $form) {
        $errors = array();

        // Global
        foreach ($form->getErrors() as $error) {
            $errors[$form->getName()][] = $error->getMessage();
        }

        // Fields
        foreach ($form as $child/** @var Form $child */) {

            if ($form->isSubmitted() && !$child->isValid()) {
                foreach ($child->getErrors() as $error) {
                    $errors[$child->getName()][] = $error->getMessage();
                }
            }
        }

        return $errors;
    }

}
