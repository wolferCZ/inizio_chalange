<?php


namespace App\To;


class HistorySearchPage {


    /**
     * @var integer
     */
    private $count;
    /**
     *
     * @var integer
     */
    private $totalCount;
    /**
     * @var array
     *
     */
    private $items;

    /**
     * @return int
     */
    public function getCount(): int {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getTotalCount(): int {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     */
    public function setTotalCount(int $totalCount): void {
        $this->totalCount = $totalCount;
    }

    /**
     * @return array
     */
    public function getItems(): array {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems(array $items): void {
        $this->items = $items;
    }


}
