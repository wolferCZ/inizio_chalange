<?php


namespace App\To;


use Symfony\Component\Validator\Constraints as Assert;

class PagingParams {

    /**
     * Sort direction.
     * @var string
     * @Assert\Choice({"ASC","DESC"})
     * @Assert\NotBlank()
     */
    private $sort;

    /**
     * Sort key
     * @var string
     * @Assert\Choice({"id","companyName", "searchedOn"})
     * @Assert\NotBlank()
     */
    private $sortBy;

    /**
     * Max number of results.
     * @var integer
     * @Assert\Type(type="integer")
     * @Assert\NotBlank()
     * @Assert\Range(min="1", max="100")
     */
    private $limit;

    /**
     * Page offset (number of results skipped).
     * @var integer
     * @Assert\Range(min="0",)
     * @Assert\NotBlank()
     * @Assert\Type(type="integer")
     */
    private $offset;

    /**
     * @return string
     */
    public function getSort() {
        return $this->sort;
    }

    /**
     * @param string $sort
     */
    public function setSort(string $sort) {
        $this->sort = $sort;
    }

    /**
     * @return string
     */
    public function getSortBy() {
        return $this->sortBy;
    }

    /**
     * @param string $sortBy
     */
    public function setSortBy(string $sortBy) {
        $this->sortBy = $sortBy;
    }

    /**
     * @return int
     */
    public function getLimit() {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit) {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset() {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset) {
        $this->offset = $offset;
    }

}
