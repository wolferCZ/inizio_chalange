<?php


namespace App\Controller;

use App\Ares\Client\AresClient;
use App\Entity\HistoryEntry;
use App\Form\PagingParamsType;
use App\Repository\HistoryEntryRepository;
use App\To\HistorySearchPage;
use App\To\PagingParams;
use App\Utils\FormErrorFormatter;
use DateTime;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AresController
 * @package App\Controller
 * @Route("/")
 */
class ApiController extends AbstractController {

    /**
     * @var AresClient
     */
    private $aresClient;

    /**
     * @var HistoryEntryRepository;
     */
    private $repository;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * AresController constructor.
     * @param AresClient $aresService
     * @param HistoryEntryRepository $repository
     * @param Serializer $serializer
     */
    public function __construct(
        AresClient $aresService,
        HistoryEntryRepository $repository,
        SerializerInterface $serializer
    ) {
        $this->aresClient = $aresService;
        $this->repository = $repository;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/ares/company/{ico}", methods={"GET"}, requirements={"ico"="\d{8}"})
     * @param string $ico
     * @return Response
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getCompany(string $ico) {
        $entry = new HistoryEntry();
        $entry->setIco($ico);
        $entry->setSearchedOn(new DateTime());

        $xml = $this->aresClient->getCompanyByIco($ico);
        $entry->setResult($xml);

        $companyXml = simplexml_load_string($xml);
        $entry->setCompanyName((string)$companyXml->xpath('//are:Obchodni_firma')[0]);

        $this->repository->create($entry);

        return new Response($this->serializer->serialize($entry, 'json'), 200,
            ['Content-type' => 'application/json']);
    }

    /**
     * Query search history
     * @Route("/history", methods={"GET"})
     */
    public function searchHistory(Request $request) {
        $pagingParams = new PagingParams();

        $form = $this->createForm(PagingParamsType::class, $pagingParams);
        $form->submit($request->query->all());

        if ($form->isSubmitted() && $form->isValid()) {
            // Map results and helper variables to response
            $page = new HistorySearchPage();
            $page->setItems($this->repository->findAllByPagingParams($form->getData()));
            $page->setCount(count($page->getItems()));
            $page->setTotalCount($this->repository->count([]));

            return new Response($this->serializer->serialize($page, 'json'), 200,
                ['Content-type' => 'application/json']);
        }

        return new JsonResponse(FormErrorFormatter::formatFormErrors($form), 400);
    }


}
