<?php

namespace App\Repository;


use App\Entity\HistoryEntry;
use App\To\PagingParams;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class CompanySearchHistoryRepository
 * @package App\Repository
 */
class HistoryEntryRepository extends ServiceEntityRepository {
    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, HistoryEntry::class);
    }

    /**
     * @param HistoryEntry $entry
     * @return HistoryEntry
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(HistoryEntry $entry) {
        $this->getEntityManager()->persist($entry);
        $this->getEntityManager()->flush($entry);
        return $entry;
    }


    /**
     * Fetch list/page of search history entries.
     * @param PagingParams $params
     * @return array list of search history entries
     */
    public function findAllByPagingParams(PagingParams $params) {

        $builder = $this->createQueryBuilder('e')
            ->orderBy(sprintf("e.%s", $params->getSortBy()), $params->getSort())
            ->setFirstResult($params->getOffset())
            ->setMaxResults($params->getLimit());

        return $builder->getQuery()->execute();
    }


}
