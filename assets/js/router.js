import CompanySearch from "./Components/CompanySearch";
import SearchHistory from "./Components/SearchHistory";
import VueRouter from "vue-router";

const routes = [


    {path: '/', redirect: '/search'},
    {path: '/search', component: CompanySearch},
    {path: '/searchHistory', component: SearchHistory},
];


const router = new VueRouter({
    routes // short for `routes: routes`
});

export default router;
